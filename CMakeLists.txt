cmake_minimum_required(VERSION 3.6)
project(myMD)
SET(CMAKE_C_COMPILER mpicc)
SET(CMAKE_CXX_COMPILER mpicxx)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g -pg")
SET(GCC_COVERAGE_LINK_FLAGS  "-g -pg")
include_directories(/usr/include/mpi/)

set(SOURCE_FILES MolSim MolSim.cpp Domain.cpp Cell.cpp OutputWriter.cpp lib/tinyxml2.cpp lib/tinyxml2.h InputReader.cpp InputReader.h TestReader.cpp TestReader.h)
SET( CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}" )
add_executable(myMD ${SOURCE_FILES})