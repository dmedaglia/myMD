#include <iostream>
#include <chrono>
#include <fstream>
#include <array>

#include <cstdlib>
#include <cassert>
#include <cstdio>
#include <cmath>


#include <mpi.h>

#include "Domain.h"
#include "TestReader.h"

using namespace std;
using namespace std::chrono;

// code taken from
//Griebel et. al.: Numerical Simulation in Molecular Dynamics, p. 427
static double GaussDeviate(double x, double y, double z, int idx) {
    double a1, a2, s, r, b1;
    static int iset = 0;
    static double b2;
    // we need to make the program deterministic, so the tests will suceeed
    // we need to get the same random numbers even distributed (where order to rand changes)
    // this may be changed but then test are indeterminitic and will fail (butterfly effect)
    srand((unsigned int) (x * 1E6 + y * 1E4 + z * 1E2 + idx));

    if (!iset) {
        do {
            a1 = 2.0 * rand() / (RAND_MAX + 1.0) - 1.0;
            a2 = 2.0 * rand() / (RAND_MAX + 1.0) - 1.0;
            r = a1 * a1 + a2 * a2;
        } while (r >= 1.0);
        s = sqrt(-2.0 * log(r) / r);
        b1 = a1 * s;
        b2 = a2 * s;
        iset = 1;
        return b1;
    } else {
        iset = 0;
        return b2;
    }
}

int Domain::getPidofProc(int x, int y, int z) {
    return x * num_procs[2] * num_procs[1] + y * num_procs[2] + z;
}
Domain::~Domain() {
    delete[] cells;
    delete[] surfaceCids;
}
Domain::Domain(InputReader* in, int world_rank, int world_size)
        : world_rank(world_rank), world_size(world_size) {
    cutOffRadius = in->r_cutoff;

    for (int axis = 0; axis < 3; axis++) {
        globalDomainSize[axis] = in->domainSize[axis];
        domainSize[axis] = in->domainSize[axis];
        low_dim[axis] = 0;
        num_procs[axis] = 1;
        proc_idx[axis] = 0;
    }
    // divide at longest
    double divide = log2(world_size);
    int curCuts = 0;
    // cut at z, then y then x
    while(pow(2, (double)curCuts) < world_size) {
        int axis = 2 - (curCuts % 3);
        num_procs[axis] *= 2;
        curCuts++;
    }

    int remainder = world_rank;
    proc_idx[0] = remainder / (num_procs[2] * num_procs[1]);
    remainder = remainder % (num_procs[2] * num_procs[1]);
    proc_idx[1] = remainder / num_procs[2];
    remainder = remainder % num_procs[2];
    proc_idx[2] = remainder;
    assert(getPidofProc(proc_idx[0], proc_idx[1], proc_idx[2]) == world_rank);  // must be invertible

    for (int axis = 0; axis < 3; axis++) {
        domainSize[axis] /= num_procs[axis];
        low_dim[axis] = domainSize[axis] * proc_idx[axis];
        high_dim[axis] = domainSize[axis] + low_dim[axis];
        containerSize[axis] = domainSize[axis] + 2;
    }
    factor_y = containerSize[0];
    factor_z = factor_y * containerSize[1];
    // alloc the cells
    numCells = containerSize[0] * containerSize[1] * containerSize[2];

#ifndef NO_OUTPUT
    cout << "init Domain: " << domainSize[0] << " | " << domainSize[1] << " | " << domainSize[2] << endl
         << "from " << low_dim[0] << " | " << low_dim[1] << " | " << low_dim[2]
         << " to " << high_dim[0] << " | " << high_dim[1] << "| " << high_dim[2] << endl
         << "proc: " << world_rank << "/" << world_size
         << " @: " << proc_idx[0] << "/" << num_procs[0] << " | " << proc_idx[1] << "/" << num_procs[1] << " | " << proc_idx[2] << "/" << num_procs[2] << endl
         << "generate " << numCells << " Cells" << endl;
#endif

    cells = new Cell[numCells];
    int innerCells = 0;
    if (domainSize[0] > 2 && domainSize[1] > 2 && domainSize[2] > 2) { // innercells must not be negative if eg 1 or 2 as sidelength passed
        innerCells =  (domainSize[0] - 2) * (domainSize[1] - 2) * (domainSize[2] - 2);;
    }
    numSurface = domainSize[0] * domainSize[1] * domainSize[2] - innerCells;

    // put the cells in the right category
    for (int z = 0; z < containerSize[2]; z++) {
        for (int y = 0; y < containerSize[1]; y++) {
            for (int x = 0; x < containerSize[0]; x++) {
                int cid = getCellIdx(x, y, z);
                //cout << "CID "<<cid << endl;
                cells[cid] = Cell(cid, x, y, z, low_dim, high_dim, cutOffRadius);
            }
        }
    }

    // initialize the neighbor offsets
    for (int i = 0; i < 26; i++) {
        neighbor_offset[i] = DIR[i][2] * factor_z + DIR[i][1] * factor_y + DIR[i][0];
    }
    // init neigbor Pids
    for (int dir = 0; dir < 26; dir++) {
        int x = (proc_idx[0] + DIR[dir][0] + num_procs[0]) % num_procs[0],
            y = (proc_idx[1] + DIR[dir][1] + num_procs[1]) % num_procs[1],
            z = (proc_idx[2] + DIR[dir][2] + num_procs[2]) % num_procs[2];
        neighborPid[dir] = getPidofProc(x, y, z);  // direction cutting is reverse, z is fastest running idx
#ifdef DIEGO_DEBUG
		cout << "Rank " << world_rank << " - Neighbor pid[" << dir << "] is: " << neighborPid[dir] << endl;
#endif
    }
    surfaceCids = new int[numSurface];

    int count_surface = 0;

    for (int i = 0; i < numCells; i++) {
        // get all the surface cells into an vector list
        if (cells[i].type == CELL_SURFACE) {
            //cout << "cell "<<i << "is surface"<<count_surface<<" of " << numSurface<<endl;
            assert(count_surface < numSurface);
            surfaceCids[count_surface] = i;
            count_surface++;
        }
    }
    // init recv buffer
    for (int i = 0; i < 26; i++) {
        recvBuffer[i] = vector<double>(1);
        sendBuffer[i] = vector<double>(1);
    }

    // push particles in  a lattice
    int pid = 0;
    double density = in->cuboid_h;   // distance between particles
     for (double z = 0; z < in->cuboid_n[2]; z++) {
        for (double y = 0; y < in->cuboid_n[1]; y++) {
            for (double x = 0; x < in->cuboid_n[0]; x++) {
                double px = x * in->cuboid_h + in->cuboid_x[0],
                       py = y * in->cuboid_h + in->cuboid_x[1],
                       pz = z * in->cuboid_h + in->cuboid_x[2];
                if (isPosInside(px, py, pz)) {

                    int cid = getCellIdxFromPos(px, py, pz);
                    //cout << "will put particle in"<<cid << endl;
                    Particle p;
                    p.pid = pid++;
                    p.pos[0] = px;
                    p.pos[1] = py;
                    p.pos[2] = pz;
                    p.f[0] = 0;
                    p.f[1] = 0;
                    p.f[2] = 0;


                    if (in->brownian_motion_init) {
                        p.vel[0] = GaussDeviate(px, py, pz, 1);   // maxwell distribution
                        p.vel[1] = GaussDeviate(px, py, pz, 2);
                        p.vel[2] = GaussDeviate(px, py, pz, 3);
                    }
                    cells[cid].particles.push_back(p);
                }
            }
        }
    }

    outputWriter = new OutputWriter(cells, numCells);
}



// checks if a Position is inside the inner cells
bool Domain::isPosInside(double x, double y, double z) {
    return (x >= low_dim[0] * cutOffRadius && x < high_dim[0] * cutOffRadius &&
            y >= low_dim[1] * cutOffRadius && y < high_dim[1] * cutOffRadius &&
            z >= low_dim[2] * cutOffRadius && z < high_dim[2] * cutOffRadius);
}

// checks if a position is inside the domain (current process) this includes the halo area
bool Domain::isPosInDomain(double x, double y, double z) {
    return (x >= (low_dim[0] - 1) * cutOffRadius && x < (high_dim[0] + 1) * cutOffRadius &&
            y >= (low_dim[1] - 1) * cutOffRadius && y < (high_dim[1] + 1) * cutOffRadius &&
            z >= (low_dim[2] - 1) * cutOffRadius && z < (high_dim[2] + 1) * cutOffRadius);
}

void Domain::calculateF() {

    for (int i = 0; i < numCells; i++) {
        cells[i].resetF();  // resets F and clears boundary
    }
    for (int i = 0; i < 26; i++) {
        sendBuffer[i].clear();
    }

    // this loop construct iterates over all owncells
    int cid = getInnerCellIdx(0, 0, 0);
    int cellPosIdx[3] = {0, 0, 0};  // index inside the domain
    for (cellPosIdx[2] = 0; cellPosIdx[2] < domainSize[2]; cellPosIdx[2]++) {
        for (cellPosIdx[1] = 0; cellPosIdx[1] < domainSize[1]; cellPosIdx[1]++) {
            for (cellPosIdx[0] = 0; cellPosIdx[0] < domainSize[0]; cellPosIdx[0]++) {
                assert(cells[cid].type != CELL_BOUNDARY);
                assert(cells[cid].type != CELL_BOUNDARY);
                cells[cid].interact();
                for (int dir = 0; dir < 13; dir++) {
                    int nid = cid + neighbor_offset[dir];

                    cells[cid].interact(&cells[nid]);

                }
                if (cells[cid].type == CELL_SURFACE) {  // need to send surfaces
                    for (int dir = 0; dir < 26; dir++) {
                        if (cells[cid].sendTo[dir]) {
                            int nid = cid + neighbor_offset[dir];
                            assert (cells[nid].type == CELL_BOUNDARY);

                            const int size = cells[cid].particles.size();
                            for (int i = 0; i < size; i++) {

                                Particle p1 = cells[cid].particles[i];  // a copy
                                // verify that they are locally and globaly on the edge
                                // the additional check at the end is necessary to not swap elements that have a edge to a periodic boundary,
                                // but not the current cell
                                for (int axis = 0; axis < 3; axis++) {
                                    if (DIR[dir][axis] == -1 && proc_idx[axis] == 0 && cellPosIdx[axis] == 0) {
                                        p1.pos[axis] += globalDomainSize[axis] * cutOffRadius;
                                    } else if (DIR[dir][axis] == 1 && proc_idx[axis] == num_procs[axis] - 1 && cellPosIdx[axis] == domainSize[axis] - 1) {
                                        p1.pos[axis] -= globalDomainSize[axis] * cutOffRadius;
                                    }
                                }

                                sendBuffer[dir].emplace_back(p1.pos[0]);
                                sendBuffer[dir].emplace_back(p1.pos[1]);
                                sendBuffer[dir].emplace_back(p1.pos[2]);

                            }
                        }
                    }
                }
                cid++;
            }
            cid += factor_y - domainSize[0];
        }
        cid += factor_z - domainSize[1] * factor_y;
    }
    // end own cells

    // communicate
    int recvlen[26] = {0, };
    MPI_Request recr[26 * 2];
    int sendlen[26] = {0, };

    for (int i = 0; i < 26; i++) {
        sendlen[i] = (int) sendBuffer[i].size();
        MPI_Isend(sendlen + i, 1, MPI_INT, neighborPid[i], i, MPI_COMM_WORLD, recr + i);
#ifdef DIEGO_DEBUG
        cout << "Rank "<< world_rank << " - sent sendlen " << sendlen[i] << " ("<< sendBuffer[i].size() << ") " << " to neighbor " <<  neighborPid[i] << " tag " << i << endl;
#endif
        /*
         * MPI_Status status;
         * MPI_Probe(neighborPid[25 - i], i, MPI_COMM_WORLD, &status);
        int number_amount;
        cout << "probed";
        MPI_Get_count(&status, MPI_INT, &number_amount);
        cout << "will recv "<<number_amount<<endl;*/
#ifdef DIEGO_DEBUG
        cout << "Rank "<< world_rank << " - receiving recvlen from neighbor " <<  neighborPid[25 - i] << " tag " << i << endl;
#endif
        MPI_Irecv(recvlen + i, 1, MPI_INT, neighborPid[25 - i], i, MPI_COMM_WORLD, recr + 26+ i);
    }

    int st = MPI_Waitall(26 * 2, recr, MPI_STATUSES_IGNORE);
#ifdef DIEGO_DEBUG
	printf("MPI_Waitall line %d returned %d\n", __LINE__, st);

	for (int i = 0; i < 26; i++){
	    cout << "Rank " << world_rank << " recvlen[" << i << "]: "<< recvlen[i] <<endl;
	}
#endif


    for (int i = 0; i < 26; i++) {

        recvBuffer[i].reserve(recvlen[i]);
#ifdef DIEGO_DEBUG
		printf("Rank %d - sending %lu to %d tag %d\n", world_rank, sendBuffer[i].size(), neighborPid[i], i);
#endif
        MPI_Isend(&sendBuffer[i][0], sendBuffer[i].size(), MPI_DOUBLE, neighborPid[i], i, MPI_COMM_WORLD,  recr + i);
#ifdef DIEGO_DEBUG
		printf("Rank %d - receiving %d from %d tag %d\n", world_rank, recvlen[i], neighborPid[25 - i], i);
#endif
        MPI_Irecv(&recvBuffer[i][0], recvlen[i], MPI_DOUBLE, neighborPid[25 - i], i, MPI_COMM_WORLD, recr + i + 26);
        // CONSIDER implementing this with MPI_Probe and not ahve the explicit wall in between
		//
    }
#ifdef DIEGO_DEBUG
        MPI_Status statuses[26*2];

        st = MPI_Waitall(26 * 2, recr, statuses);
      printf("MPI_Waitall line %d returned %d\n", __LINE__, st);

      for (int i = 0; i < 26*2; i++)
      {
          printf("Mpi status %d:\n", i);
          printf("\tsource: %d:\n", statuses[i].MPI_SOURCE);
          printf("\ttag: %d:\n", statuses[i].MPI_TAG);
          printf("\terror: %d:\n", statuses[i].MPI_ERROR);
      }

    #else
    st = MPI_Waitall(26 * 2, recr, MPI_STATUSES_IGNORE);
#endif


    // insert the particle
    for (int i = 0; i < 26; i++) {
        if (recvlen[i] > 0) {
            //cout << "ranke"<<world_rank<<"got " << recvlen[i] / 3 << " from " << neighborPid[i] << "at dir " << i << endl;
            for (int j = 0; j < recvlen[i]; j += 3) {

                // checks if recieving particle is in boundary
                assert(!isPosInside(recvBuffer[i][j], recvBuffer[i][j + 1], recvBuffer[i][j + 2]));
                assert(isPosInDomain(recvBuffer[i][j], recvBuffer[i][j + 1], recvBuffer[i][j + 2]));
                Particle p;
                p.pos[0] = recvBuffer[i][j];
                p.pos[1] = recvBuffer[i][j + 1];
                p.pos[2] = recvBuffer[i][j + 2];
p.pid = 10000+j;    //DEBUG ONLY
                int insertCid = getCellIdxFromPos(p.pos[0], p.pos[1], p.pos[2]);
                assert(insertCid >= 0 && insertCid < numCells);
                assert(cells[insertCid].type == CELL_BOUNDARY);
                //cout << "insert" << insertCid << "are " << cells[insertCid].particles.size() << endl;
                cells[insertCid].particles.push_back(p);
            }
        }
    }

    // foreign cells interaction, from the surface to the outside
    for (int i = 0; i < numSurface; i++) {
        cid = surfaceCids[i];
        for (int dir = 0; dir < 26; dir++) {
            int nid = cid + neighbor_offset[dir];
            if (cells[nid].type == CELL_BOUNDARY) {
                //cout << "boundary check " << cid << " to "<<nid << endl;
                cells[cid].interact(&cells[nid]);
            }
        }
    }
}

void Domain::calculateX(double dt) {
    for (int i = 0; i < 26; i++) {
        sendBuffer[i].clear();
        recvBuffer[i].clear();
    }
    // this loop construct iterates over all owncells
    int cid = getInnerCellIdx(0, 0, 0);
    int cellPosIdx[3] = {0, 0, 0};  // index inside the domain
    for (cellPosIdx[2] = 0; cellPosIdx[2] < domainSize[2]; cellPosIdx[2]++) {
        for (cellPosIdx[1] = 0; cellPosIdx[1] < domainSize[1]; cellPosIdx[1]++) {
            for (cellPosIdx[0] = 0; cellPosIdx[0] < domainSize[0]; cellPosIdx[0]++) {

                cells[cid].calculateX(dt);
                // will now rehash
                int size = cells[cid].particles.size();
                for (int i = 0; i < size; i++) {
                    Particle* p = &cells[cid].particles[i];
                    // check if particle is still in this domain
                    if (isPosInside(p->pos[0], p->pos[1], p->pos[2])) {
                        int dst_cid = getCellIdxFromPos(p->pos);

                        if (dst_cid != cid) {   // move within the domain

                            cells[dst_cid].particles.push_back(cells[cid].particles[i]);
                            cells[cid].particles[i] = cells[cid].particles.back();
                            cells[cid].particles.pop_back();
                            --i;
                            --size;
                        }
                    } else {
                        // move to different process
                        int nPid = world_rank;
                        int dirOffset[3];
                        for (int axis = 0; axis < 3; axis++) {
                            // CONSIDER writing this with floor, but benchmark
                            if (p->pos[axis] < low_dim[axis] * cutOffRadius) {
                                dirOffset[axis] = -1;
                            } else if (p->pos[axis] >= high_dim[axis] * cutOffRadius) {
                                dirOffset[axis] = 1;
                            } else {
                                dirOffset[axis] = 0;
                            }
                        }
                        // got the direction i need to send this along, -> derive the direction index
                        int dirIdx = (dirOffset[0] + 1) + (dirOffset[1] + 1) * 3 + (dirOffset[2] + 1) * 9;
                        if (dirIdx >= 13 ) dirIdx--;    // calc out 0,0,0, which does not exist
                        assert(DIR[dirIdx][0] == dirOffset[0]);
                        assert(DIR[dirIdx][1] == dirOffset[1]);
                        assert(DIR[dirIdx][2] == dirOffset[2]);
                        for (int axis = 0; axis < 3; axis++) {
                            // TODO: find more performant way todo this periodic check
                            double sendVal = p->pos[axis];
                            if (dirOffset[axis] == -1 && proc_idx[axis] == 0 && cellPosIdx[axis] == 0) {
                                sendVal += globalDomainSize[axis] * cutOffRadius;
                            } else if (dirOffset[axis] == 1 && proc_idx[axis] == num_procs[axis] - 1 && cellPosIdx[axis] == domainSize[axis] - 1) {
                                sendVal -= globalDomainSize[axis] * cutOffRadius;
                            }
                            sendBuffer[dirIdx].push_back(sendVal);
                        }
                        sendBuffer[dirIdx].push_back(p->f[0]);
                        sendBuffer[dirIdx].push_back(p->f[1]);
                        sendBuffer[dirIdx].push_back(p->f[2]);
                        sendBuffer[dirIdx].push_back(p->vel[0]);
                        sendBuffer[dirIdx].push_back(p->vel[1]);
                        sendBuffer[dirIdx].push_back(p->vel[2]);

                        // remove the particle
                        cells[cid].particles[i] = cells[cid].particles.back();
                        cells[cid].particles.pop_back();
                        --i;
                        --size;
                    }
                }

                cid++;
            }
            cid += factor_y - domainSize[0];
        }
        cid += factor_z - domainSize[1] * factor_y;
    }
    // end own cells

    // communicate
    int recvlen[26];
    MPI_Request recr[26 * 2];
	int st;

    for (int i = 0; i < 26; ++i) {
        int sendlen = (int) sendBuffer[i].size();
        MPI_Isend(&sendlen, 1, MPI_INT, neighborPid[i], 0, MPI_COMM_WORLD, recr + i);

        MPI_Irecv(recvlen + i, 1, MPI_INT, neighborPid[i], 0, MPI_COMM_WORLD, recr + 26 + i);
    }

    st = MPI_Waitall(26 * 2, recr, MPI_STATUSES_IGNORE);
#ifdef DIEG_DEBUG
	printf("MPI_Waitall line %d returned %d\n", __LINE__, st);
#endif
    for (int i = 0; i < 26; i++) {

        MPI_Isend(&sendBuffer[i][0], sendBuffer[i].size(), MPI_DOUBLE, neighborPid[i], 0, MPI_COMM_WORLD, recr + i);

        recvBuffer[i].reserve(recvlen[i]);
        //Recv
        MPI_Irecv(&recvBuffer[i][0], recvlen[i], MPI_DOUBLE, neighborPid[i], 0, MPI_COMM_WORLD, recr + i + 26);

    }
    st = MPI_Waitall(26 * 2, recr, MPI_STATUSES_IGNORE); // TODO this is wrong as not every pair is send see checks above
#ifdef DIEG_DEBUG
    printf("MPI_Waitall line %d returned %d\n", __LINE__, st);
#endif

    for (int i = 0; i < 26; i++) {
        for (int j = 0; j < recvlen[i]; j += 9) {
            Particle p;
            p.pos[0] = recvBuffer[i][j + 0];
            p.pos[1] = recvBuffer[i][j + 1];
            p.pos[2] = recvBuffer[i][j + 2];
            p.f[0] = recvBuffer[i][j + 3];
            p.f[1] = recvBuffer[i][j + 4];
            p.f[2] = recvBuffer[i][j + 5];
            p.vel[0] = recvBuffer[i][j + 6];
            p.vel[1] = recvBuffer[i][j + 7];
            p.vel[2] = recvBuffer[i][j + 8];
            assert(isPosInside(p.pos[0], p.pos[1], p.pos[2]));
p.pid = 2000000 + j;    // TMP DEBUG
            int dstCId = getCellIdxFromPos(p.pos);
            cells[dstCId].particles.push_back(p);
        }
    }

    st = MPI_Waitall(26 * 2, recr, MPI_STATUSES_IGNORE);
#ifdef DIEGO_DEBUG
	printf("MPI_Waitall line %d returned %d\n", __LINE__, st);
#endif
}

void Domain::calculateV(double dt) {
    typedef std::vector<Particle>::iterator iter;
    // this loop construct iterates over all owncells
    int cid = getInnerCellIdx(0, 0, 0);
    for (int z = 0; z < domainSize[2]; z++) {
        for (int y = 0; y < domainSize[1]; y++) {
            for (int x = 0; x < domainSize[0]; x++) {
                const iter end = cells[cid].particles.end();
                for (iter it = cells[cid].particles.begin(); it != end; ++it) {
                    double mass = 15;   // TMP hardcodedd
                    double factor = dt / (mass * 2.0);

                    it->vel += factor * (it->f + it->old_f);

                }
                cid++;
            }
            cid += factor_y - domainSize[0];
        }
        cid += factor_z - domainSize[1] * factor_y;
    }
    // end own cells

}

int Domain::getInnerCellIdx(int x, int y, int z) {
    return getCellIdx(x + 1, y + 1, z + 1); // inner cells have an offset of 1 (halo)
}

// get the cellIdx in the array
int Domain::getCellIdx(int x, int y, int z) {
    return z * factor_z + y * factor_y + x;
}
// calculate the cellIdx from the position
int Domain::getCellIdxFromPos(double x, double y, double z) {
    assert(isPosInDomain(x, y, z));
    x += (1 - low_dim[0]) * cutOffRadius;  // convert from local coordinates to global coos
    y += (1 - low_dim[1]) * cutOffRadius;
    z += (1 - low_dim[2]) * cutOffRadius;

    return getCellIdx(x / cutOffRadius, y / cutOffRadius, z / cutOffRadius);
}
int Domain::getCellIdxFromPos(utils::Vector<double, 3> pos) {
    return getCellIdxFromPos(pos[0], pos[1], pos[2]);
}

void Domain::plotParticles(int iteration, string outFilename) {
    string postFix = "_";
    if (iteration < 10) postFix += "0";
    if (iteration < 100) postFix += "0";

    string write_filename = "out/" + outFilename + "_" + to_string(world_rank) + postFix + to_string(iteration) + ".vtu";
    outputWriter->write(write_filename, true);

// DIEGO: uncomment to enable the checker for correct output
//    TestReader* test = new TestReader(cells, numCells);
//    string test_filename = "test_ref/" + outFilename + "_0" + postFix + to_string(iteration) + ".vtu";
//    test->readFile(test_filename, true);
}

void Domain::applyThermostat(double target_temp) {
    typedef std::vector<Particle>::iterator iter;
    int numParticles = 0;
    double ekin = 0;
    int numCells = 0;
    int cid = getInnerCellIdx(0, 0, 0);
    for (int z = 0; z < domainSize[2]; z++) {
        for (int y = 0; y < domainSize[1]; y++) {
            for (int x = 0; x < domainSize[0]; x++) {
                numParticles += cells[cid].particles.size();
                ekin += cells[cid].getEnergyDouble();
                numCells++;
                cid++;
            }
            cid += factor_y - domainSize[0];
        }
        cid += factor_z - domainSize[1] * factor_y;
    }
    ekin /= 2;  // 1/2*m*v^2
    // end own cells

    MPI_Allreduce(MPI_IN_PLACE, &ekin, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    MPI_Allreduce(MPI_IN_PLACE, &numParticles, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    int size = 3 * numParticles;    // the 3 is the harcoded dimenstion

    //division by dimension and number particles
    //prevent 0 division
	//DIEGO: this happens at np=16.
	if (ekin <= 0)
		return;
    //assert(ekin > 0);

    double current_temp = ekin * 2 / ((double) size);

    double max_delta_temp = 10; // ? where does this come from?

    //cap with max_delta
    double factor = sqrt(
            ((target_temp < current_temp) ?
             max(target_temp, current_temp - max_delta_temp) : min(target_temp, current_temp + max_delta_temp))
            / current_temp);
#if !NDEBUG
    cout << "ekin is " << ekin << ", curTemp: " << current_temp << " to targetT: " << target_temp
         << " and has " << numParticles << "particles in " << numCells << "Cells" << endl
         << "themostat will apply factor " << factor << endl;
#endif


    // scaling of velocity
    cid = getInnerCellIdx(0, 0, 0);
    for (int z = 0; z < domainSize[2]; z++) {
        for (int y = 0; y < domainSize[1]; y++) {
            for (int x = 0; x < domainSize[0]; x++) {
                const iter end = cells[cid].particles.end();
                for (iter it = cells[cid].particles.begin(); it != end; ++it) {
                    it->vel *= factor;
                }
                cid++;
            }
            cid += factor_y - domainSize[0];
        }
        cid += factor_z - domainSize[1] * factor_y;
    }
    // end own cells
}


