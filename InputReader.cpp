//
// Created by andi on 30.12.16.
//

#include <cstdlib>
#include <cstring>
#include <iostream>
#include "InputReader.h"

InputReader::InputReader(string filename) {
    XMLDocument doc;
    doc.LoadFile(filename.c_str());
    if (doc.ErrorID() != 0) {
        cerr << "Cannot load file " << filename << endl;
        exit(-1);
    }

    const XMLElement* inp = doc.FirstChildElement("input");
    delta_t = getDouble(inp, "delta_t");
    end_time = getDouble(inp, "end_time");
    outputname = getString(inp, "outputname");
    writefrequency = getInt(inp, "writefrequency");
    r_cutoff = getDouble(inp, "r_cutoff");
    brownian_motion_init = getString(inp, "brownian_motion_init") != "false";

    const XMLElement* inpTemp = inp->FirstChildElement("temperature_info");
    if (inpTemp != nullptr) {
        thermostat_active = true;
        init_temp = getInt(inpTemp, "init_temp");
        n_thermostat = getInt(inpTemp, "n_thermostat");
        target_temp = getInt(inpTemp, "target_temp");
        cout << "Thermostat active initT: " << init_temp << " every " << n_thermostat << " steps, to " << target_temp << endl;
    }
    const XMLElement* inpCube = inp->FirstChildElement("cuboids_info")->FirstChildElement("cuboid");
    if (inpCube != nullptr) {
        cout << "there is a cuboid" << endl;
        getVector(inpCube, "x", &(cuboid_x[0]));
        getVector(inpCube, "n", &(cuboid_n[0]));
        cuboid_h = getDouble(inpCube, "h");

    } else {
        cerr << "input needs to have exactly one cube" << endl;
        exit(-1);
    }
    getVector(inp, "domain_size", &(domainSize[0]));

    cout << "vec is "<<domainSize[0]<< " " << domainSize[1] << " " << domainSize[2] << endl;
}
double InputReader::getDouble(const XMLElement* in, const char* name) {
    const XMLElement * el = in->FirstChildElement(name);
    if (el == nullptr) {
        cerr << "ERROR, cannot read " << name << endl;
        exit(-1);
    } else {
        return atof(el->GetText());
    }
}

void InputReader::getVector(const XMLElement* in, const char* name, int* vec) {
    const XMLElement* el = in->FirstChildElement(name);
    const XMLElement* fst = el->FirstChildElement("value");
    vec[0] = atoi(fst->GetText());
    const XMLElement* second = fst->NextSiblingElement("value");
    vec[1] = atoi(second->GetText());
    const XMLElement* third = second->NextSiblingElement("value");
    vec[2] = atoi(third->GetText());
}

void InputReader::getVector(const XMLElement* in, const char* name, double* vec) {
    const XMLElement* el = in->FirstChildElement(name);
    const XMLElement* fst = el->FirstChildElement("value");
    vec[0] = atof(fst->GetText());
    const XMLElement* second = fst->NextSiblingElement("value");
    vec[1] = atof(second->GetText());
    const XMLElement* third = second->NextSiblingElement("value");
    vec[2] = atof(third->GetText());
}


int InputReader::getInt(const XMLElement* in, const char* name) {
    const XMLElement * el = in->FirstChildElement(name);
    if (el == nullptr) {
        cerr << "ERROR, cannot read " << name << endl;
        exit(-1);
    } else {
        return atoi(el->GetText());
    }
}

string InputReader::getString(const XMLElement* inp, const char* name){
    const XMLElement * el = inp->FirstChildElement(name);
    if (el == nullptr) {
        cerr << "ERROR, cannot read " << name << endl;
        exit(-1);
    } else {
        const char* val = el->GetText();
        return string(val, strlen(val));
    }
}
