//
// Created by andi on 30.12.16.
//

#ifndef MYMD_INPUTREADER_H
#define MYMD_INPUTREADER_H

#include <string>
#include "lib/tinyxml2.h"
using namespace std;
using namespace tinyxml2;

class InputReader {
public:
    string outputname;
    double delta_t, end_time, r_cutoff;
    int writefrequency;
    int domainSize[3];
    bool thermostat_active = false;
    bool brownian_motion_init;
    int init_temp = 0, n_thermostat = 0, target_temp = 0;
    double cuboid_x[3], cuboid_h;
    int cuboid_n[3];
    InputReader(string filename);
    double getDouble(const XMLElement* in, const char* name);
    int getInt(const XMLElement* in, const char* name);
    string getString(const XMLElement* inp, const char* name);
    void getVector(const XMLElement* in, const char* name, int* vec);
    void getVector(const XMLElement* in, const char* name, double* vec);
};

#endif //MYMD_INPUTREADER_H
