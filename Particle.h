#include <iostream>
using namespace std;

#ifndef PARTICLE_H_
#define PARTICLE_H_

class Particle {
public:
    utils::Vector<double, 3> pos, f, vel, old_f;
    int pid;
    Particle() { pos = 0; f = 0; old_f = 0; vel = 0; pid = -1;}
    Particle(utils::Vector<double, 3> pos) {
        this->pos = pos;
        f = 0;
    }

    void show() {
        cout << "P:" << pid << endl
             << "pos: [" << pos[0]<<" | "<<pos[1]<<" | "<<pos[2] <<"] V: ["
             << vel[0] << " | "<< vel[1] << " | "<< vel[2] << "]" << endl <<
             " curF: [" << f[0] << " | "<< f[1] << " | "<< f[2] << "]"
             " oldF: [" << old_f[0] << " | "<< old_f[1] << " | "<< old_f[2] << "]" << endl;
    }
};



#endif