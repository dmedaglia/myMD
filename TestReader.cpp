//
// Created by andi on 27.01.17.
//
#include <fstream>
#include <cassert>
#include "TestReader.h"
#include <mpi.h>

/*
 * this TestReader will write a txt file that is easy to read in order to mnake testing between different
 * implementations easier
 * it matches the current results with the one of a precomputed file
 * it has a very bad time comeplexity so dont call it too often and reduce the test dataset
 */

TestReader::TestReader(Cell* cells, int numCells):
    numCells(numCells), cells(cells) {
    cout << "TestReader initialized with" << numCells;

};

void TestReader::readFile(string filename, bool excludeBoundary) {
    XMLDocument doc;

    cout << "Test against " << filename << endl;
    doc.LoadFile(filename.c_str());
    if (doc.ErrorID() != 0) {
        cerr << "Cannot load file " << filename << endl;
        exit(-1);
    }

    const XMLElement* vtk = doc.FirstChildElement("VTKFile");
    const XMLElement* unstruc = vtk->FirstChildElement("UnstructuredGrid");
    const XMLElement* piece = unstruc->FirstChildElement("Piece");
    int numPoints = 0;
    piece->QueryIntAttribute("NumberOfPoints", &numPoints);

    // check Numparticels matches
    int sum = 0;
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY || !excludeBoundary)
            sum += cells[i].particles.size();
    }
    MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    assert(numPoints == sum);

    const XMLElement* point = piece->FirstChildElement("PointData");
    const XMLElement* mass = point->FirstChildElement("DataArray");
    assert(mass != nullptr);
    const XMLElement* cid = mass->NextSiblingElement("DataArray");
    assert(cid != nullptr);
    const XMLElement* cell_type = cid->NextSiblingElement("DataArray");
    assert(cell_type != nullptr);
    const XMLElement* forces = cell_type->NextSiblingElement("DataArray");
    assert(forces != nullptr);
    verifyForces(forces, numPoints);
    const XMLElement* velocities = forces->NextSiblingElement("DataArray");
    assert(velocities != nullptr);
    const XMLElement* positions = velocities->NextSiblingElement("DataArray");
    assert(positions != nullptr);
    verifyPositions(positions, numPoints);
    cout << "test successfull" << endl;
}


void TestReader::verifyForces(const XMLElement* forceEl, int numParticles) {
    const char* val = forceEl->GetText();
    string forces_str = string(val, strlen(val));
    stringstream force_ss(forces_str);
    typedef std::vector<Particle>::iterator iter;


    double eps = 1E-8;
    double relError;
    double forces[numParticles * 3];
    for (int i = 0; i < numParticles * 3; i++) {
        force_ss >> forces[i];
    }
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {

                if (!find(it->f[0], it->f[1], it->f[2], forces, numParticles)) {
                    cerr << "could not find force " << it->f[0] << " " << it->f[1] << " " << it->f[2] << endl;
                    exit(-1);
                }
            }
        }
    }
}

void TestReader::verifyPositions(const XMLElement* positionEl, int numParticles) {
    const char* val = positionEl->GetText();
    string forces_str = string(val, strlen(val));
    stringstream force_ss(forces_str);
    typedef std::vector<Particle>::iterator iter;


    double eps = 1E-8;
    double relError;
    double positions[numParticles * 3];
    for (int i = 0; i < numParticles * 3; i++) {
        force_ss >> positions[i];
    }
    for (int i = 0; i < numCells; i++) {
        if (cells[i].type != CELL_BOUNDARY) {
            const iter end = cells[i].particles.end();
            for (iter it = cells[i].particles.begin(); it != end; ++it) {

                if (!find(it->pos[0], it->pos[1], it->pos[2], positions, numParticles)) {
                    cerr << "could not find position" << it->pos[0] << " " << it->pos[1] << " " << it->pos[2] << endl;
                    exit(-1);
                }
            }
        }
    }
}

// The tests try to match to the closest vector
// this could lead to unexpected ehaviour that if the vectos do not match to themselves but to one that is very close
// might cause unexpected behavior
// yet it is very unlikely that a vec matches to not it one and the other has a too large epsilon
// especially for position this wont happen
bool TestReader::find(double x, double y, double z, double* arr, int numParticles) {
    bool found = false;

    for (int j = 0; j < numParticles; j++) {

        double val = arr[j * 3];

        double absError = abs(x - val);
        double relError;
        if (val == 0) {
            relError = absError;    // avoid division by zero, in this case we take absError
        } else {
            relError = absError / abs(val);
        }
        if (relError > MAX_ERR) continue;
// Y
        val = arr[j * 3 + 1];
        absError = abs(y - val);
        if (val == 0) {
            relError = absError;    // avoid division by zero, in this case we take absError
        } else {
            relError = absError / abs(val);
        }
        if (relError > MAX_ERR) continue;
// Z

        val = arr[j * 3 + 2];
        absError = abs(z - val);
        if (val == 0) {
            relError = absError;    // avoid division by zero, in this case we take absError
        } else {
            relError = absError / abs(val);
        }
        if (relError < MAX_ERR) {
            // cout << "Did fine" << x <<" " << y << " " << z<< " in " <<arr[j*3]<<  endl;
            found = true;
            arr[j * 3] = arr[numParticles * 3 - 3];
            arr[j * 3 + 1] = arr[numParticles * 3 - 2];
            arr[j * 3 + 2] = arr[numParticles * 3 - 1];    // mark as used, by removing
            j = numParticles;   // exit j loop
            numParticles--;
        }
    }

    return found;
}

